package com.example.CartFilterPOC.predicates;

import com.example.CartFilterPOC.model.CartLinesToEvaluate;

public interface CartLinePredicate<I> {

    boolean test(I node, CartLinesToEvaluate cartLinesToEvaluate);
}
