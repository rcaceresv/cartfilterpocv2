package com.example.CartFilterPOC.predicates;

import com.example.CartFilterPOC.model.CartLinesToEvaluate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProductSitesPredicate implements CartLinePredicate<List<String>> {
    @Override
    public boolean test(List<String> node, CartLinesToEvaluate cartLinesToEvaluate) {
        return sitesTest(node, cartLinesToEvaluate);
    }

    private boolean sitesTest(List<String> node, CartLinesToEvaluate cartLinesToEvaluate) {
        if (node == null) return true;
        return node.containsAll(cartLinesToEvaluate.getProductSites());

    }
}
