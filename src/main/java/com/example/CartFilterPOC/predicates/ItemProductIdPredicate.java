package com.example.CartFilterPOC.predicates;

import com.example.CartFilterPOC.model.CartLinesToEvaluate;
import com.example.CartFilterPOC.model.Item;
import org.springframework.stereotype.Component;

@Component
public class ItemProductIdPredicate implements CartLinePredicate<Item> {

    @Override
    public boolean test(Item item, CartLinesToEvaluate cartLinesToEvaluate) {
        return productIdTest(item,cartLinesToEvaluate) &&
               variantIdTest(item,cartLinesToEvaluate) &&
               offeringIdTest(item,cartLinesToEvaluate) &&
               sellerIdTest(item,cartLinesToEvaluate);
    }

    private boolean productIdTest(Item item, CartLinesToEvaluate cartLinesToEvaluate) {
        if (cartLinesToEvaluate.getProductId() == null) return true;
        return item.getProductId().equals(cartLinesToEvaluate.getProductId());
    }

    private boolean variantIdTest(Item item, CartLinesToEvaluate cartLinesToEvaluate) {
        if (cartLinesToEvaluate.getVariantId() == null) return true;
        return item.getVariantId().equals(cartLinesToEvaluate.getVariantId());
    }

    private boolean offeringIdTest(Item item, CartLinesToEvaluate cartLinesToEvaluate) {
        if (cartLinesToEvaluate.getOfferingId() == null) return true;
        return item.getOfferingId().equals(cartLinesToEvaluate.getOfferingId());
    }

    private boolean sellerIdTest(Item item, CartLinesToEvaluate cartLinesToEvaluate) {
        if (cartLinesToEvaluate.getSellerId() == null) return true;
        return item.getSellerId().equals(cartLinesToEvaluate.getSellerId());
    }
}
