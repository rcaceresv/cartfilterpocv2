package com.example.CartFilterPOC;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CartFilterPocApplication {

	public static void main(String[] args) {
		SpringApplication.run(CartFilterPocApplication.class, args);
	}

}
