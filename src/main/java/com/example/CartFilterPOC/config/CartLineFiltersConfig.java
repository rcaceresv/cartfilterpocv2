package com.example.CartFilterPOC.config;

import com.example.CartFilterPOC.enums.FilterNamesEnum;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

@Configuration
@ConfigurationProperties(prefix = "config")
@Data
public class CartLineFiltersConfig {

    List<FilterSetting> cartLineFilters;


    @Data
    public static class FilterSetting {
        private FilterNamesEnum name;
        private Map<String, String> fields;
    }


    public Optional<FilterNamesEnum> getFilterName(String keyField) {
        Predicate<FilterSetting> matchField = f -> f.getFields().containsKey(keyField);
        return cartLineFilters.stream().filter(matchField).map(f -> f.getName()).findFirst();
    }

    public String getPathField(String keyParam) {
        String pathField = null;
        for (FilterSetting fs : cartLineFilters) {
            pathField = fs.getFields().entrySet().stream()
                    .filter(e -> e.getKey().equals(keyParam))
                    .map(Map.Entry::getValue)
                    .findFirst()
                    .orElse(null);
            if (pathField != null) return pathField;
        }
        return pathField;
    }
}
