package com.example.CartFilterPOC.manager;

import com.example.CartFilterPOC.config.CartLineFiltersConfig;
import com.example.CartFilterPOC.enums.FilterNamesEnum;
import com.example.CartFilterPOC.exceptions.InvalidParameterException;
import com.example.CartFilterPOC.model.CartLines;
import com.example.CartFilterPOC.model.CartLinesToEvaluate;
import com.example.CartFilterPOC.model.CartPredicate;
import com.example.CartFilterPOC.filters.IFilter;
import com.example.CartFilterPOC.model.FilterRequest;
import com.example.CartFilterPOC.services.CartService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
public class CartFilterManager {

    @Autowired
    private CartLineFiltersConfig cartLineFiltersConfig;
    @Autowired
    private CartService cartService;
    @Autowired
    private List<IFilter> filters;

    public CartLines handleCartFilter(Map<String, String> filters) {

        FilterRequest filterRequest = FilterRequest.builder().build();
        filterUrlMapper(filterRequest, filters);
        setCart(filterRequest);
        filters.forEach((filterTofind, matchValue) -> {
            setPredicate(filterTofind, filterRequest);
            applyFilter(filterRequest);
            System.out.println();
        });

        return filterRequest.getCartLines();
    }


    private void setCart(FilterRequest filterRequest) {
        filterRequest.setCartLines(cartService.getCartLines());
    }

    private void filterUrlMapper(FilterRequest request, Map<String, String> filter) {
        Map<String, String> mapToParse = new HashMap<>();
        filter.forEach((key, value) -> {
            String evaluatorKey = cartLineFiltersConfig.getPathField(key);
            mapToParse.put(evaluatorKey, value);
        });

        CartLinesToEvaluate cartLinesToEvaluate = new ObjectMapper().convertValue(mapToParse, CartLinesToEvaluate.class);
        CartPredicate cartPredicate = CartPredicate.builder().cartLinesToEvaluate(cartLinesToEvaluate).build();
        request.setCartPredicate(cartPredicate);
    }

    private void setPredicate(String keyField, FilterRequest filterRequest) {
        Optional<FilterNamesEnum> filterName = cartLineFiltersConfig.getFilterName(keyField);
        if (!filterName.isPresent()) throw new InvalidParameterException();
        filterRequest.getCartPredicate().setFilterName(filterName.get());
    }

    private void applyFilter(FilterRequest filterRequest) {
        for (IFilter<CartLines, FilterRequest> filter : filters) {
            filterRequest.setCartLines(filter.execute(filterRequest));
        }
    }


}
