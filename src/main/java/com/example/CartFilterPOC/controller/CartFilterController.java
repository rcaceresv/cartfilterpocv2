package com.example.CartFilterPOC.controller;

import com.example.CartFilterPOC.config.CartLineFiltersConfig;
import com.example.CartFilterPOC.manager.CartFilterManager;
import com.example.CartFilterPOC.model.CartLines;
import com.example.CartFilterPOC.services.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Map;

@RestController
@RequestMapping("/v1/carts")
public class CartFilterController {

    @Autowired
    private CartFilterManager cartFilterManager;

    @GetMapping("/filter")
    public CartLines obtainCartFilteringBy(@RequestParam Map filters) throws IOException {
        return cartFilterManager.handleCartFilter(filters);
    }

}
