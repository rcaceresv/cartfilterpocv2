package com.example.CartFilterPOC.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Item {

    private String productId;
    private String variantId;
    private String offeringId;
    private String sellerId;
    private List<String> productSites;
}
