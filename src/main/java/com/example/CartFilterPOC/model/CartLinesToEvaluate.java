package com.example.CartFilterPOC.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CartLinesToEvaluate {
    private String cartLineId;
    private String cartLineNumber;
    private String productId;
    private String variantId;
    private String offeringId;
    private String sellerId;
    private List<String> productSites;


    public void setProductSites(String productSitesString) {
        String[] elements = productSitesString.split(",");
        this.productSites = Arrays.asList(elements);
    }
}

