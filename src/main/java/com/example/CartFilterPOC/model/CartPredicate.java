package com.example.CartFilterPOC.model;

import com.example.CartFilterPOC.enums.FilterNamesEnum;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CartPredicate {
    private FilterNamesEnum filterName;
    private CartLinesToEvaluate cartLinesToEvaluate;

}
