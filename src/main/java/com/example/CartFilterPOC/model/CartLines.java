package com.example.CartFilterPOC.model;

import lombok.Data;

import java.util.List;

@Data
public class CartLines {

    private List<CartLine> cartLines;


}
