package com.example.CartFilterPOC.model;

import lombok.Data;

@Data
public class CartLine {
    private String cartLineId;
    private String cartLineNumber;
    private Item item;
}
