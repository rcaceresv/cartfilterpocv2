package com.example.CartFilterPOC.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FilterRequest {
    private CartPredicate cartPredicate;
    private CartLines cartLines;
}
