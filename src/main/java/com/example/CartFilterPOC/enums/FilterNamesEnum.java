package com.example.CartFilterPOC.enums;

public enum FilterNamesEnum {
    CART_LINES_FILTER,
    ITEM_FILTER,
    PRODUCT_SITES_FILTER
}
