package com.example.CartFilterPOC.filters;

public interface IFilter<O,I> {

    O execute(I input);
}
