package com.example.CartFilterPOC.filters;

import com.example.CartFilterPOC.model.CartLine;
import com.example.CartFilterPOC.model.CartLines;
import com.example.CartFilterPOC.model.FilterRequest;
import com.example.CartFilterPOC.model.Item;
import com.example.CartFilterPOC.predicates.CartLinePredicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.example.CartFilterPOC.enums.FilterNamesEnum.ITEM_FILTER;


@Service
public class ItemFilter implements IFilter<CartLines, FilterRequest> {
    @Autowired
    private CartLinePredicate<Item> itemProductIdPredicate;

    @Override
    public CartLines execute(FilterRequest request) {
        CartLines cartLines = new CartLines();
        List<CartLine> cartLinesFiltered = request.getCartLines().getCartLines();
        if (isFilterValid(request)) {
            cartLinesFiltered = request.getCartLines()
                    .getCartLines().stream()
                    .filter(item -> itemProductIdPredicate.test(item.getItem(), request.getCartPredicate().getCartLinesToEvaluate()))
                    .collect(Collectors.toList());
        }
        cartLines.setCartLines(cartLinesFiltered);
        return cartLines;
    }


    private boolean isFilterValid(FilterRequest request) {
        return ITEM_FILTER.equals(request.getCartPredicate().getFilterName());
    }
}
