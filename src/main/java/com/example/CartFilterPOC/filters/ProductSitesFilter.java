package com.example.CartFilterPOC.filters;

import com.example.CartFilterPOC.model.CartLine;
import com.example.CartFilterPOC.model.CartLines;
import com.example.CartFilterPOC.model.FilterRequest;
import com.example.CartFilterPOC.model.Item;
import com.example.CartFilterPOC.predicates.CartLinePredicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.example.CartFilterPOC.enums.FilterNamesEnum.PRODUCT_SITES_FILTER;

@Component
public class ProductSitesFilter implements IFilter<CartLines, FilterRequest> {

    @Autowired
    private CartLinePredicate<List<String>> productSitesPredicate;

    @Override
    public CartLines execute(FilterRequest request) {
        CartLines cartLines = new CartLines();
        List<CartLine> cartLinesFiltered = request.getCartLines().getCartLines();
        if (isFilterValid(request)) {
            cartLinesFiltered = request.getCartLines().getCartLines().stream()
                    .filter(cl -> productSitesPredicate.test(cl.getItem().getProductSites(), request.getCartPredicate().getCartLinesToEvaluate()))
                    .collect(Collectors.toList());

        }
        cartLines.setCartLines(cartLinesFiltered);
        return cartLines;
    }


    private boolean isFilterValid(FilterRequest request) {
        return PRODUCT_SITES_FILTER.equals(request.getCartPredicate().getFilterName());
    }
}
