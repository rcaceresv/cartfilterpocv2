package com.example.CartFilterPOC.services;

import com.example.CartFilterPOC.model.CartLines;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;

@Service
public class CartService {


    public CartLines getCartLines() {
        try {
            return loadMockCart();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private CartLines loadMockCart() throws IOException, URISyntaxException {
        ObjectMapper objectMapper = new ObjectMapper();
        ClassLoader classLoader = getClass().getClassLoader();
        URL resource = classLoader.getResource("mock/cart_1.json");
        Path fileName = Path.of(resource.toURI());
        String actual = Files.readString(fileName);
        return objectMapper.readValue(actual, CartLines.class);
    }

}
